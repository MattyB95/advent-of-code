FABRIC_SIZE = 1000
file = open("input.txt", "r")
fabric = [[0 for column in range(FABRIC_SIZE)] for row in range(FABRIC_SIZE)]
for line in file:
    claim = line.split()
    claimId = int(claim[0].replace("#", ""))
    coordinates = claim[2].split(",")
    leftEdge = int(coordinates[0])
    topEdge = int(coordinates[1].replace(':', ''))
    size = claim[3].split("x")
    wide = int(size[0])
    tall = int(size[1])
    xRange = range(leftEdge, leftEdge + wide)
    for xIndex in xRange:
        yRange = range(topEdge, topEdge + tall)
        for yIndex in yRange:
            if fabric[xIndex][yIndex] == 0:
                fabric[xIndex][yIndex] = claimId
            else:
                fabric[xIndex][yIndex] = -1
overlap = sum(squareInch.count(-1) for squareInch in fabric)
print(overlap)
