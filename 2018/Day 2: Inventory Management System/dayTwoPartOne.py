from collections import Counter

twoLetters = 0
threeLetters = 0
file = open("input.txt", "r")
for line in file:
    counter = Counter(''.join(line))
    values = counter.values()
    if 2 in values:
        twoLetters += 1
    if 3 in values:
        threeLetters += 1
print(twoLetters)
print(threeLetters)
print(twoLetters * threeLetters)
