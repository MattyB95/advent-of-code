from difflib import SequenceMatcher

similarityMatrix = []
fileOne = open("input.txt", "r")
for lineOne in fileOne:
    fileTwo = open("input.txt", "r")
    for lineTwo in fileTwo:
        if lineOne != lineTwo:
            similarity = SequenceMatcher(None, lineOne, lineTwo).ratio()
            similarityMatrix.append([similarity, lineOne, lineTwo])
similarityMatrix.sort()
mostSimilarBoxId = similarityMatrix.pop()
firstId = mostSimilarBoxId[1]
secondId = mostSimilarBoxId[2]
commonLetters = ""
for index, letter in enumerate(firstId):
    if letter == secondId[index]:
        commonLetters += letter
print(commonLetters)
