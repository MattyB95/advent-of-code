frequency = 0
frequencyList = {frequency}
found = False
while not found:
    file = open("input.txt", "r")
    for line in file:
        frequency += int(line)
        if frequency in frequencyList:
            print(frequency)
            found = True
            break
        frequencyList.add(frequency)
